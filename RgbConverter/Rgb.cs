﻿using System;

namespace RgbConverter
{
    public static class Rgb
    {
        public static string GetHexRepresentation(int red, int green, int blue)
        {
            red = CheckCondition(red);
            green = CheckCondition(green);
            blue = CheckCondition(blue);

            return string.Concat(ConvertFrom10To16(red), ConvertFrom10To16(green), ConvertFrom10To16(blue));
        }

        public static int CheckCondition(int number)
        {
            if (number > 255)
            {
                return 255;
            }

            if (number < 0)
            {
                return 0;
            }

            return number;
        }

        public static string ConvertFrom10To16(int number)
        {
            char[] template = new char[16] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

            if (number < 16)
            {
                return string.Concat('0', template[number]);
            }

            int[] remainder = new int[2] { 0, 0 };
            int copyNumber = number;

            while (number > 16)
            {
                number /= 16;
                remainder[1] = copyNumber - (number * 16);
                remainder[0] = number;
            }

            return string.Concat(template[remainder[0]], template[remainder[1]]);
        }
    }
}
